package com;

import java.util.stream.IntStream;

public class Task1 {
    public static void main(String[] args) {
        int sumNumbers = IntStream.rangeClosed(1,100)
                .filter(n -> n % 2 == 0)
                .sum();
        System.out.println(sumNumbers);
        }
    }

