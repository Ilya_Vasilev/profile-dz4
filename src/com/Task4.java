package com;

import java.util.Collections;
import java.util.List;

public class Task4 {
    public static void main(String[] args) {
        List<Integer> list = List.of(3, 5, 76, 4, 13, 23);
        list.stream()
                .sorted(Collections.reverseOrder())
                .forEach(System.out::println);
    }
}
